#include <SPI.h>

#define pwmCurrent 3
#define outCSB 4
#define outRS 5
#define inSelButton 6
#define inDecButton 7
#define inIncButton 8
#define pwmVoltage 9
#define outOutputOnLed 14
#define in24V 15
#define adcVoltage A6
#define adcCurrent A7

#define VOLTAGE_SCALE_12V 2.0
#define VOLTAGE_SCALE_24V 4.0
#define CURRENT_SCALE 1.25

bool outputOn = false;
int voltageSet = 0;
int currentSet = 0;
float voltageScaleFactor = VOLTAGE_SCALE_12V;
int currentSetOffset = 0;
float currentOutOffset = 0;

SPISettings settings(100000, MSBFIRST, SPI_MODE3);

void transferByte(byte data, bool rsLow = true) {
  SPI.beginTransaction(settings);

  digitalWrite(outCSB, LOW);
  digitalWrite(outRS, rsLow ? LOW : HIGH);
  delay(1);

  SPI.transfer(data);
  
  digitalWrite(outCSB, HIGH);
  
  SPI.endTransaction();
}

void initLcd() {
  transferByte(0);
  transferByte(0b00111001);
  delay(10);
  transferByte(0b00011100);
  transferByte(0b01010010);
  transferByte(0b01101001);
  transferByte(0b01110100);
  transferByte(0b00111000);
  delay(10);
  transferByte(0b00001100);
  transferByte(0b00000001);
  transferByte(0b00000110);
}

void setLcdPos(byte line, byte pos) {
  transferByte(0b10000000 + line * 0x40 + pos);
}

void writeLcdString(byte line, byte pos, const char* str, int len) {
  setLcdPos(line, pos);
  for (int i = 0; i < len; i++) {
    transferByte(str[i], false);
  }
}

void lcdWriteStatics() {
  writeLcdString(0, 2, "V", 1);
  writeLcdString(1, 1, "mA", 2);
}

void setup() {
  // port setup
  analogWrite(pwmVoltage, 0);
  analogWrite(pwmCurrent, 0);
  pinMode(outCSB, OUTPUT);
  pinMode(outRS, OUTPUT);
  pinMode(inSelButton, INPUT_PULLUP);
  pinMode(inDecButton, INPUT_PULLUP);
  pinMode(inIncButton, INPUT_PULLUP);
  pinMode(outOutputOnLed, OUTPUT);
  pinMode(in24V, INPUT);
  SPI.begin();
  
  delay(100);
  updateVoltageScale();
  measureMinCurrentReading();

  // init
  initLcd();
  lcdWriteStatics();
  Serial.begin(9600);
  Serial.write("ready\n");
}

void lcdUpdateSelector(byte line) {
  char arrow = 0x7E;
  writeLcdString((line + 1) % 2, 0, " ", 1);
  writeLcdString(line, 0, &arrow, 1);
}

void lcdUpdateValue(byte line, byte pos, String valueStr) {
  const int maxLen = 5;
  const int valueLen = valueStr.length();
  char lcdStr[maxLen] = {0};
  strncpy(lcdStr + maxLen - valueLen, valueStr.c_str(), valueLen);
  writeLcdString(line, pos, lcdStr, maxLen);
}

void lcdUpdateVoltsSet(float value) {
  lcdUpdateValue(0, 5, String(value, 2));
}

void lcdUpdateVoltsOut(float value) {
  lcdUpdateValue(0, 11, String(value, 2));
}

void lcdUpdateMAmpsSet(int value) {
  lcdUpdateValue(1, 5, String(value));
}

void lcdUpdateMAmpsOut(int value) {
  lcdUpdateValue(1, 11, String(value));
}

float convertAdcToVolts(int value) {
  return value * 5.0 * voltageScaleFactor / 1024.0;
}

float convertAdcToMAmps(int value) {
  return value * 1000.0 * CURRENT_SCALE / 1024.0; // 1000 mA for the full 5V output range
}

int convertVoltsToPwm(float value) {
  return value * 255.0 / 5.0 / voltageScaleFactor;
}

int convertMAmpsToPwm(float value) {
  return value * 255.0 / 500.0 / CURRENT_SCALE; // 500 mA for the full 5V output range
}

float convertPwmToVolts(int value) {
  return value * 5.0 * voltageScaleFactor / 255.0;
}

float convertPwmToMAmps(int value) {
  return value * 500.0 * CURRENT_SCALE / 255.0; // 500 mA for the full 5V output range
}

float getVoltage() {
  return convertAdcToVolts(analogRead(adcVoltage));
}

float getCurrent() {
  return convertAdcToMAmps(analogRead(adcCurrent));
}

void measureMinCurrentReading() {
  float c1 = getCurrent();
  delay(100);
  float c2 = getCurrent();
  float mean = (c1 + c2) / 2.0;
  
  currentSetOffset = convertMAmpsToPwm(mean) + 1;
  currentOutOffset = mean;
}

void updateVoltageScale() {
  if (digitalRead(in24V) == LOW) {
    voltageScaleFactor = VOLTAGE_SCALE_12V;
  }
  else {
    voltageScaleFactor = VOLTAGE_SCALE_24V;
  }
}

void onOutputOn() {
  // update zero current reading
  measureMinCurrentReading();
  // turn on LED
  digitalWrite(outOutputOnLed, HIGH);
}

void onOutputOff() {
  // turn off LED
  digitalWrite(outOutputOnLed, LOW);
  // reset PWMs
  analogWrite(pwmVoltage, 0);
  analogWrite(pwmCurrent, 0);
}

byte handleButtons() {
  static byte selector = 0; // volts
  static int* pSelectedControl = &voltageSet;
  static int selButtonPressCounter = 0;

  if (digitalRead(inDecButton) == LOW) {
    int dec = 1;
    if (digitalRead(inSelButton) == LOW) {
      dec = 10;
    }
    *pSelectedControl -= dec;
  }
  else if (digitalRead(inIncButton) == LOW) {
    int inc = 1;
    if (digitalRead(inSelButton) == LOW) {
      inc = 10;
    }
    *pSelectedControl += inc;
  }
  else if (digitalRead(inSelButton) == LOW) {
    if (selButtonPressCounter++ == 0) {
      selector = (selector + 1) % 2;
      switch (selector) {
        case 0: pSelectedControl = &voltageSet; break;
        case 1: pSelectedControl = &currentSet; break;
      }
    }
    else if (selButtonPressCounter == 10 /* ±1 sec */) {
      outputOn = outputOn ? false : true;
      if (outputOn) {
        onOutputOn();
      }
      else {
        onOutputOff();
      }
    }
  }
  else {
    selButtonPressCounter = 0;
  }

  return selector;
}

bool handleSerial() {
  static String command;
  static bool logging = false;
  
  if (Serial.available() > 0) {
    char character = Serial.read();
    if (character != '\n') {
      command.reserve(8);
      command += character;
    }
    else {
      if (command == "on") {
        outputOn = true;
        onOutputOn();
      }
      else if (command == "off") {
        outputOn = false;
        onOutputOff();
      }
      else if (command.startsWith("v:")) {
        float volts = command.substring(2).toFloat();
        voltageSet = convertVoltsToPwm(volts);
      }
      else if (command.startsWith("c:")) {
        float mAmps = command.substring(2).toFloat();
        currentSet = convertMAmpsToPwm(mAmps);
      }
      else if (command == "log") {
        logging = true;
      }
      else if (command == "nolog") {
        logging = false;
      }
      command = "";
    }
  }

  return logging;
}

void logOutputValues(float voltage, int current) {
  static byte intervalCounter = 0;

  if ((intervalCounter++ % 10) == 0) {
    String out = String(voltage, 2) + "," + String(current) + "\n";
    Serial.write(out.c_str());
  }
}

void checkSetLimits() {
  voltageSet = max(voltageSet, 0);
  voltageSet = min(voltageSet, 255);
  currentSet = max(currentSet, 0);
  currentSet = min(currentSet, 255 - currentSetOffset);
}

void loop() {
  // check controls
  byte selector = handleButtons();
  bool logging = handleSerial();
  checkSetLimits();
  
  // update display
  lcdUpdateSelector(selector);
  lcdUpdateVoltsSet(convertPwmToVolts(voltageSet));
  lcdUpdateMAmpsSet(convertPwmToMAmps(currentSet));
  float voltage = getVoltage();
  lcdUpdateVoltsOut(voltage);
  float current = getCurrent() - currentOutOffset;
  lcdUpdateMAmpsOut(current);

  if (outputOn) {
    // set/update PWMs
    analogWrite(pwmVoltage, voltageSet);
    analogWrite(pwmCurrent, currentSet + currentSetOffset);
  }

  if (logging) {
    logOutputValues(voltage, current);
  }
  
  delay(100);
}
