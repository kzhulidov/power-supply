#!/usr/bin/env python2

import time
import sys
import serial

# open the port
ps = serial.Serial(port=sys.argv[1], baudrate=9600, timeout=10)
assert ps.read_until() == 'ready\n'

# set voltage and current
ps.write('v:5\n')
ps.write('c:200\n')

# on
ps.write('on\n')

time.sleep(10)

# off
ps.write('off\n')

